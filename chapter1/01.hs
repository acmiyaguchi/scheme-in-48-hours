module Main where
import System.Environment

-- Modify the original program to read two arguments from the command line
-- and then prints out a message using both of them
main :: IO ()
main = do
    args <- getArgs
    putStrLn ("Hello, " ++ args !! 0 ++ " " ++ args !! 1)
