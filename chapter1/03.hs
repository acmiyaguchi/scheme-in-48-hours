module Main where
import System.Environment

-- Change the program so it prompts for a name, reads the name, and returns it
-- as a string instead of a command line value
main :: IO ()
main = do
    putStrLn ("What is your name?")
    name <- getLine
    putStrLn ("Hello, " ++ name)
