module Main where
import System.Environment

-- Change the program so that it performs simple arithmetic on both of the
-- arguments and prints out the results
main :: IO ()
main = do
    -- Do addition on the first two arguments of the commandline
    args <- getArgs
    putStrLn (show((read (args !! 0)) + (read (args !! 1))))
