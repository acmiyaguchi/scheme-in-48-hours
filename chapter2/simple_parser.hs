import Text.ParserCombinators.Parsec hiding (spaces)
import System.Environment
import Control.Monad

-- Define the datatype that can hold any lisp value
data LispVal = Atom String
    | List [LispVal]
    | DottedList [LispVal] LispVal
    | Number Integer
    | String String
    | Bool Bool

-- Define a parser that recognizes one of the symbols allowed in Scheme
-- identifiers.
symbol :: Parser Char
symbol = oneOf "!#$%&|*+-/:<=>?@^_~"

spaces :: Parser ()
spaces = skipMany1 space

-- 5. Create a Character constructor and create a parser for character literals

-- 6. Add spport for R5RS syntax for decimals

-- 7. Add parsers to support the full numeric tower of Scheme numeric types.

-- 2. Change parseString so that \" gives a literal quote character instead of 
-- terminating the string. Replace noneOf "\"" with new parser action that 
-- accepts either non-quote or backslash followed by quote mark.
-- 3. Also modify to support \n, \t, \r, \\ and any other escape character
escapedCharacter :: Parser Char
escapedCharacter = do
    char '\\'
    x <- oneOf "\\\"" -- backslash or doublequote
    return x

parseString :: Parser LispVal
parseString = do
    char '"'
    x <- many $ escapedCharacter <|> noneOf "\"\\"
    char '"'
    return $ String x

parseAtom :: Parser LispVal
parseAtom = do
    first <- letter <|> symbol
    rest <- many (letter <|> digit <|> symbol)
    let atom = first:rest
    return $ case atom of
        "#t" -> Bool True
        "#f" -> Bool False
        _    -> Atom atom

-- 1. Rewrite parseNumber without using liftM using
--  a. do-notation
    {-
    parseNumber :: Parser LispVal
    parseNumber = do
        num <-  many1 digit
        return $ Number (read num)
    -}

--  b. explicit sequencing using >>= operator
    {-
    parseNumber :: Parser LispVal
    parseNumber = many1 digit >>= return . Number . read
    -}

-- 4. Change to support Scheme standard for different bases
parseNumber :: Parser LispVal
parseNumber = liftM (Number . read) $ many1 digit

parseExpr :: Parser LispVal
parseExpr = parseAtom
    <|> parseString
    <|> parseNumber

-- Call our parser and handle errors
readExpr :: String -> String
readExpr input = case parse parseExpr "lisp" input of
    Left err -> "No Match: " ++ show err
    Right val -> "Found value"

main :: IO ()
main = do
    args <- getArgs
    putStrLn (readExpr (args !! 0))
